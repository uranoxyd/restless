# Restless

A few convenience functions to create HTTP API's

# Usage Example

```go
package main

import (
	"log"
	"net/http"

	"gitlab.com/uranoxyd/restless"
)

func demoHandlerOK(w http.ResponseWriter, r *http.Request) {
	restless.WriteDataResponse(w, "Hello World")
}
func demoHandlerError(w http.ResponseWriter, r *http.Request) {
	restless.WriteErrorResponse(w, "my_error", "my error description")
}

func main() {
	http.HandleFunc("/", demoHandlerOK)
	http.HandleFunc("/error", demoHandlerError)
	log.Fatal(http.ListenAndServe("127.0.0.1:8000", nil))
}
```

``http://127.0.0.1:8000/`` returns:

```json
{
    "status": "ok",
    "data": "Hello World"
}
```

``http://127.0.0.1:8000/error`` returns:

```json
{
    "status": "error",
    "error": {
        "id": "my_error",
        "msg": "my error description"
    }
}
```
