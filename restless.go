// Copyright 2022 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package restless

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

var ShowErrorDetails bool = false

type ApiResponseError struct {
	ID      string `json:"id"`
	Message string `json:"msg"`
}

func (e *ApiResponseError) Error() string {
	return fmt.Sprintf("%s - %s", e.ID, e.Message)
}

type ApiResponse struct {
	Status     string                 `json:"status"`
	Error      *ApiResponseError      `json:"error,omitempty"`
	Data       any                    `json:"data,omitempty"`
	Pagination *ApiResponsePagination `json:"pagination,omitempty"`
}

type ApiResponsePagination struct {
	Page     int `json:"page"`
	Pages    int `json:"pages"`
	PageSize int `json:"pageSize"`
}

func prepareResponse(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	prepareResponse(w)
	w.WriteHeader(http.StatusNotFound)
	WriteErrorResponse(w, "not_found", "unknown endpoint")
}

func WriteResponse(w http.ResponseWriter, httpStatusCode int, response *ApiResponse) {
	prepareResponse(w)
	js, err := json.Marshal(response)
	if err != nil {
		if ShowErrorDetails {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			http.Error(w, "internal server error", http.StatusInternalServerError)
		}
		return
	}
	w.WriteHeader(httpStatusCode)
	w.Write(js)
}

func WriteDataResponse(w http.ResponseWriter, data any) {
	WriteResponse(w, http.StatusOK, &ApiResponse{
		Status: "ok",
		Data:   data,
	})
}
func WriteDataPaginatedResponse(w http.ResponseWriter, data any, page int, pages int, pageSize int) {
	WriteResponse(w, http.StatusOK, &ApiResponse{
		Status:     "ok",
		Data:       data,
		Pagination: &ApiResponsePagination{Page: page, Pages: pages, PageSize: pageSize},
	})
}
func WriteErrorResponse(w http.ResponseWriter, id string, message string) {
	WriteResponse(w, http.StatusOK, &ApiResponse{
		Status: "error",
		Error:  &ApiResponseError{ID: id, Message: message},
	})
}
func WriteErrorResponseWithData(w http.ResponseWriter, id string, message string, data any) {
	WriteResponse(w, http.StatusOK, &ApiResponse{
		Status: "error",
		Error:  &ApiResponseError{ID: id, Message: message},
		Data:   data,
	})
}
func WriteErrorResponseWithCode(w http.ResponseWriter, httpStatusCode int, id string, message string) {
	WriteResponse(w, httpStatusCode, &ApiResponse{
		Status: "error",
		Error:  &ApiResponseError{ID: id, Message: message},
	})
}
func WriteErrorResponseWithCodeAndData(w http.ResponseWriter, httpStatusCode int, id string, message string, data any) {
	WriteResponse(w, httpStatusCode, &ApiResponse{
		Status: "error",
		Error:  &ApiResponseError{ID: id, Message: message},
		Data:   data,
	})
}

func WriteUnauthorizedResponse(w http.ResponseWriter) {
	prepareResponse(w)
	w.WriteHeader(http.StatusUnauthorized)
	WriteErrorResponse(w, "unathorized", "Not authorized")
}
func WriteUnauthorizedResponseMessage(w http.ResponseWriter, id string, message string) {
	prepareResponse(w)
	w.WriteHeader(http.StatusUnauthorized)
	WriteErrorResponse(w, id, message)
}
func WriteNotFoundResponse(w http.ResponseWriter) {
	prepareResponse(w)
	w.WriteHeader(http.StatusNotFound)
	WriteErrorResponse(w, "not_found", "Not found")
}

// WriteInternalServerError writes “internal_error“ error response, if DEBUG is true the error string is sent
func WriteInternalServerError(w http.ResponseWriter, err error) {
	if err != nil {
		if ShowErrorDetails {
			WriteErrorResponseWithCode(w, http.StatusInternalServerError, "internal_error", fmt.Sprint(err))
		} else {
			WriteErrorResponseWithCode(w, http.StatusInternalServerError, "internal_error", "Internal server error")
		}
		return
	}
}

func ScanRequestBody(r *http.Request, target any) (int, error) {
	bodyData, err := io.ReadAll(r.Body)
	if err != nil {
		return 0, err
	}

	err = json.Unmarshal(bodyData, target)
	if err != nil {
		return 0, err
	}

	return len(bodyData), nil
}

func ReadResponse(r io.Reader) (*ApiResponse, error) {
	responseBytes, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}

	response := &ApiResponse{}
	if err := json.Unmarshal(responseBytes, response); err != nil {
		return nil, err
	}

	if response.Status == "error" {
		return response, response.Error
	}

	return response, nil
}
