// Copyright 2022 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package restless

import (
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http/httptest"
	"strings"
	"testing"
)

type ErrorReader struct {
	io.ReadCloser
}

func (r ErrorReader) Read(p []byte) (n int, err error) {
	return 0, fmt.Errorf("this error is expected")
}
func (r ErrorReader) Close() error {
	return nil
}

func Test_NotFoundHandler(t *testing.T) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)

	NotFoundHandler(w, r)

	if w.Code != 404 {
		t.Error("wrong status code")
	}

	if w.Body.String() != `{"status":"error","error":{"id":"not_found","msg":"unknown endpoint"}}` {
		t.Error(w)
	}
}
func Test_WriteResponse(t *testing.T) {
	ShowErrorDetails = false // set to false just in case

	// normal test
	apiresponse := &ApiResponse{
		Status: "I'm a teapot",
		Error: &ApiResponseError{
			ID:      "1",
			Message: "Hyper Text Coffee Pot Control Protocol",
		},
		Data: []byte("f"),
		Pagination: &ApiResponsePagination{
			Page:     1,
			Pages:    1,
			PageSize: 1,
		},
	}
	w := httptest.NewRecorder()
	WriteResponse(w, 418, apiresponse)
	err := json.Unmarshal(w.Body.Bytes(), apiresponse)
	if err != nil {
		t.Error(err)
	}
	if w.Body.String() != `{"status":"I'm a teapot","error":{"id":"1","msg":"Hyper Text Coffee Pot Control Protocol"},"data":"Zg==","pagination":{"page":1,"pages":1,"pageSize":1}}` {
		t.Error(w.Body.String())
	}

	// test against error
	w = httptest.NewRecorder()
	WriteResponse(w, 418, &ApiResponse{
		Status: "I'm a teapot",
		Error: &ApiResponseError{
			ID:      "1",
			Message: "Hyper Text Coffee Pot Control Protocol",
		},
		Data: math.Inf(1),
		Pagination: &ApiResponsePagination{
			Page:     1,
			Pages:    1,
			PageSize: 1,
		},
	})
	if w.Code != 500 {
		t.Error("does not return error code 500")
	}

	// test against error with details
	ShowErrorDetails = true
	w = httptest.NewRecorder()
	WriteResponse(w, 418, &ApiResponse{
		Status: "I'm a teapot",
		Error: &ApiResponseError{
			ID:      "1",
			Message: "Hyper Text Coffee Pot Control Protocol",
		},
		Data: math.Inf(1),
		Pagination: &ApiResponsePagination{
			Page:     1,
			Pages:    1,
			PageSize: 1,
		},
	})
	if w.Code != 500 {
		t.Error("does not return error code 500")
	}
}
func Test_WriteDataResponse(t *testing.T) {
	w := httptest.NewRecorder()
	WriteDataResponse(w, "Hello World")

	if w.Body.String() != `{"status":"ok","data":"Hello World"}` {
		t.Error("body wrong content")
	}
}
func Test_WriteDataPaginatedResponse(t *testing.T) {
	w := httptest.NewRecorder()
	var data []string = []string{"bla"}
	WriteDataPaginatedResponse(w, data, 1, 1, 1)

	var jsonParse struct {
		Status     string   `json:"status"`
		Data       []string `json:"data"` // <-- in this case []string
		Pagination struct {
			Page     int `json:"page"`
			Pages    int `json:"pages"`
			PageSize int `json:"pageSize"`
		} `json:"pagination"`
	}

	err := json.Unmarshal(w.Body.Bytes(), &jsonParse)
	if err != nil {
		t.Error(err)
	}
}
func Test_WriteErrorResponseWithData(t *testing.T) {
	w := httptest.NewRecorder()
	WriteErrorResponseWithData(w, "42", "I'm a teapot", []string{"test"})

	var check *ApiResponse
	err := json.Unmarshal(w.Body.Bytes(), &check)
	if err != nil {
		t.Error(err)
	}

	if check.Error.ID != "42" {
		t.Error("id does not match with provided data")
	}
	if check.Status != "error" {
		t.Errorf("status != error")
	}
	if fmt.Sprint(check.Data) != "[test]" {
		t.Error(check.Data)
	}
}
func Test_WriteErrorResponseWithCode(t *testing.T) {
	w := httptest.NewRecorder()
	WriteErrorResponseWithCode(w, 418, "42", "testmessage101")

	var check *ApiResponse
	err := json.Unmarshal(w.Body.Bytes(), &check)
	if err != nil {
		t.Error(err)
	}

	if w.Body.String() != `{"status":"error","error":{"id":"42","msg":"testmessage101"}}` {
		t.Error("reply does not match expected body", w.Body.String())
	}
}
func Test_WriteErrorResponseWithCodeAndData(t *testing.T) {
	w := httptest.NewRecorder()
	WriteErrorResponseWithCodeAndData(w, 418, "42", "testmessage101", []string{"test"})

	var check *ApiResponse
	err := json.Unmarshal(w.Body.Bytes(), &check)
	if err != nil {
		t.Error(err)
	}

	if w.Body.String() != `{"status":"error","error":{"id":"42","msg":"testmessage101"},"data":["test"]}` {
		t.Error("reply does not match expected body", w.Body.String())
	}
}
func Test_WriteUnauthorizedResponse(t *testing.T) {
	w := httptest.NewRecorder()
	WriteUnauthorizedResponse(w)
	if w.Body.String() != `{"status":"error","error":{"id":"unathorized","msg":"Not authorized"}}` {
		t.Error("wrong body in reply")
	}
	if w.Code != 401 {
		t.Error("wrong http error status code")
	}
}
func Test_WriteUnauthorizedResponseMessage(t *testing.T) {
	w := httptest.NewRecorder()
	WriteUnauthorizedResponseMessage(w, "42", "testmessage")
	if w.Body.String() != `{"status":"error","error":{"id":"42","msg":"testmessage"}}` {
		t.Error("wrong body in reply")
	}
	if w.Code != 401 {
		t.Error("wrong http error status code")
	}
}
func Test_WriteNotFoundResponse(t *testing.T) {
	w := httptest.NewRecorder()
	WriteNotFoundResponse(w)
	if w.Body.String() != `{"status":"error","error":{"id":"not_found","msg":"Not found"}}` {
		t.Error("wrong body in reply")
	}
	if w.Code != 404 {
		t.Error("wrong http error status code")
	}
}
func Test_WriteInternalServerError(t *testing.T) {
	ShowErrorDetails = false // set to false just in case

	w := httptest.NewRecorder()
	WriteInternalServerError(w, fmt.Errorf("testerror"))
	if w.Body.String() != `{"status":"error","error":{"id":"internal_error","msg":"Internal server error"}}` {
		t.Error("wrong body in reply")
	}
	if w.Code != 500 {
		t.Error("wrong http error status code")
	}

	ShowErrorDetails = true
	w = httptest.NewRecorder()
	WriteInternalServerError(w, fmt.Errorf("testerror"))
	if w.Body.String() != `{"status":"error","error":{"id":"internal_error","msg":"testerror"}}` {
		t.Error("wrong body in reply", w.Body.String())
	}
	if w.Code != 500 {
		t.Error("wrong http error status code")
	}
}
func Test_ScanRequestBody(t *testing.T) {
	{
		r := httptest.NewRequest("GET", "/home", ErrorReader{})

		var target []string
		scan, err := ScanRequestBody(r, &target)
		if err == nil {
			t.Error("expected error")
		}
		if scan != 0 {
			t.Error("return value is not correct lenght 14 !=", scan)
		}
	}

	{
		r := httptest.NewRequest("GET", "/home", strings.NewReader(`["teststring"]`))

		var target []string
		scan, err := ScanRequestBody(r, &target)
		if err != nil {
			t.Error(err)
		}
		if scan != 14 {
			t.Error("return value is not correct lenght 14 !=", scan)
		}
	}

	{
		r := httptest.NewRequest("GET", "/home", strings.NewReader(`["teststring"]`))

		var target []int
		scan, err := ScanRequestBody(r, &target)
		if err == nil {
			t.Error("should not work")
		}
		if scan != 0 {
			t.Error("return value is not correct lenght 0 != ", scan)
		}
	}

}
